import { browser, by, element } from 'protractor';
browser.waitForAngularEnabled(false);

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    // Returns h1 text to check that app has rendered
    return element(by.css('app-root h1')).getText();
  }

  getDronesTable() {
    // Returns object matrix with on screen information
    // Depending if the API is online or offline
    let displayed;
    if (element(by.css('#api_offline')).isPresent()) {
      displayed = {
        offline_message: true,
        table_data: false
      };
    } else {
      displayed = {
        offline_message: false,
        table_data: true
      };
    }
    return displayed;
  }
}
