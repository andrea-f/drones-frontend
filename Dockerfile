FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY dist/drones-frontend/ .
COPY src/assets/config_prod.ts /usr/share/nginx/html/assets/config.ts