import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../assets/config';

@Injectable({
  providedIn: 'root'
})
export class DronesService {
  dronesUrl = config.apiUrl;
  constructor(private http: HttpClient) {}

  getDrones() {
    return this.http.get(this.dronesUrl);
  }
}
