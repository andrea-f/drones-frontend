import { Component, OnInit, OnDestroy } from '@angular/core';
import { Drone } from '../drone';
import { DronesService } from '../drones.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-drones',
  templateUrl: './drones.component.html',
  styleUrls: ['./drones.component.css']
})
export class DronesComponent implements OnInit, OnDestroy {

  getDrones;
  drones: Array<Drone>;
  pollingTime = 1000; // in ms
  pollerId;
  errorMessage: string;

  constructor(
    private dronesService: DronesService,
  ) {
    this.errorMessage = 'LOADING DATA...';
  }

  poller() {
    // Recreates the component so that the data is updated.
    this.pollerId = setInterval(() => {
      this.populateTable();
    }, this.pollingTime);
  }

  populateTable() {
    this.getDrones = this.dronesService.getDrones().subscribe((drones: Array<Drone>) => {
      // Load drones information from the server
      this.drones = drones.map((drone: Drone) => new Drone(drone));
    },
    error => {
      this.errorMessage = 'BACKEND IS OFFLINE.';
      console.log(error); // error path
    });
  }

  ngOnInit() {
    // Initial data fetch
    // Start polling for data
    this.poller();
  }

  ngOnDestroy() {
    if (typeof this.getDrones !== 'undefined') {
      // Unsubscribe form the service
      this.getDrones.unsubscribe();
    }
    // Clear the current interval
    clearInterval(this.pollerId);
  }
}
