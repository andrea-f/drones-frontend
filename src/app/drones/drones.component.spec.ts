import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DronesService } from '../drones.service';
import { DronesComponent } from './drones.component';
import { defer } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Drone } from '../drone';

let httpClientSpy: { get: jasmine.Spy };

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('DronesComponent', () => {
  let component: DronesComponent;
  let fixture: ComponentFixture<DronesComponent>;


  beforeEach(async(() => {

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

    TestBed.configureTestingModule({
      declarations: [ DronesComponent ],
      providers: [
        {provide: HttpClient, useValue: httpClientSpy},
        DronesService
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(DronesComponent);
    component = fixture.componentInstance;
  }));

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create <table> with data drone has_not_moved is false', () => {
    const d = new Drone({
      drone_id: 999,
      speed: 10,
      locations: [{
        lat: 5,
        long: 5,
        timestamp: 12345
      }],
      has_not_moved: false
    });
    component.drones = [d];
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('td').textContent).toContain(d.lat);
  });
  it('should create <table> with data drone has_not_moved is true', () => {
    const d = new Drone({
      drone_id: 999,
      speed: 10,
      locations: [{
        lat: 5,
        long: 5,
        timestamp: 12345
      }],
      has_not_moved: true
    });
    component.drones = [d];
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('tbody tr').style.backgroundColor).toContain('red');
  });
});
