export class Drone {
  id: number;
  lat: number;
  lng: number;
  speed: number;
  has_not_moved: boolean;
  timestamp: string;

  constructor(private drone_obj) {
    // Get the last location
    const last_location = (drone_obj.locations.length - 1);
    this.id = drone_obj.drone_id;
    this.lat = drone_obj.locations[last_location].lat;
    this.lng = drone_obj.locations[last_location].long;
    this.timestamp = drone_obj.locations[last_location].timestamp;
    this.speed = drone_obj.speed;
    this.has_not_moved = drone_obj.has_not_moved;
  }
}
