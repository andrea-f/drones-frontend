import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { DronesComponent } from './drones/drones.component';
import { HttpClient } from '@angular/common/http';
import { DronesService } from './drones.service';
import { of, from} from 'rxjs';

let httpClientSpy: { get: jasmine.Spy };

describe('AppComponent', () => {

  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        DronesComponent
      ],
      providers: [
        {provide: HttpClient, useValue: httpClientSpy},
        DronesService
      ],

    }).compileComponents();
    httpClientSpy.get.and.returnValue(of([]));

  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Drones Monitoring Dashboard'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Drones Monitoring Dashboard');
  }));
});
