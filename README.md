# Drones Frontend

This is an Angular 6 frontend to the Drones service. The backend API is polled every 1s to check for updates.
The drones which have not moved 1 meter in the past 10 seconds are highlighted in red.

## Development server

Install dependencies with `npm install`.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## How to start

1. Unzip the repo
2. cd into `drones-frontend`
3. Run `docker build -it drones-frontend .`
4. Run the whole app by running the `docker-compose` file in the `drones-backend` part of the application, with: `docker-compose up -d`
