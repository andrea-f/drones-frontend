import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display dashboard title', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Drones Monitoring Dashboard');
  });

  it('should get the data in the table if backend is running, otherwise not', () => {
    page.navigateTo();
    const apiStatus = page.getDronesTable();
    if (apiStatus.offline_message) {
      expect(apiStatus.table_data).toBe(false);
  } else {
    expect(apiStatus.table_data).toBe(true);
  }
  });
});
