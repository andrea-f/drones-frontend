import { TestBed, inject } from '@angular/core/testing';
import { DronesService } from './drones.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

let httpClientSpy: { get: jasmine.Spy };

describe('DronesService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

    TestBed.configureTestingModule({
      providers: [
        DronesService,
        {provide: HttpClient, useValue: httpClientSpy}
      ]
    });
  });

  it('service can be created', inject([DronesService], (service: DronesService) => {
    expect(service).toBeTruthy();
  }));

  it('Drones service can GET drones list', inject([DronesService], (service: DronesService) => {
    const expectedData = [];
    httpClientSpy.get.and.returnValue(of(expectedData));
    service.getDrones().subscribe(
      data => expect(data).toBe(expectedData),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  }));
});
